package com.softserve.edu.chessboard;

public class Main {

    public static void main(String[] args) {
        try {
            int[] params = InputValidator.validateParams(args);
            if (params != null) {
                System.out.println(new ChessboardPrinter(new ChessBoard(params[0], params[1])).printChessboard());
            }
        } catch (NumberFormatException e) {
            System.out.print("Strings are not allowed!");
        } catch (IllegalArgumentException e) {
            System.out.print("Params should be more than zero!");
        }
    }

}
