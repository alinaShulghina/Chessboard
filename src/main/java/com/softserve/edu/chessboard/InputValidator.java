package com.softserve.edu.chessboard;

/*
    Class for input validation of String[] args
 */
public class InputValidator {

    /*
        Method for parsing string params to integer
     */
    public static int[] validateParams(String[] inputParams) throws NumberFormatException {
        if (inputParams.length < 2) {
            System.out.println("You must input 2 params: width and height! Seems like you forgot about height");
            return null;
        }
        int width = Integer.parseInt(inputParams[0]);
        int height = Integer.parseInt(inputParams[1]);
        if (width <= 0 | height <= 0) {
            System.out.println("Your params must be more than zero!");
            return null;
        }
        return new int[]{width, height};
    }
}
