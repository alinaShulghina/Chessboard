package com.softserve.edu.chessboard;

/**
 * Created by alin- on 14.12.2017.
 */
public class ChessboardPrinter {

    private ChessBoard chessBoard;

    public ChessboardPrinter(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }

    public String printChessboard() {
        if (chessBoard.getWidth() <= 0 | chessBoard.getHeight() <= 0) {
            throw new IllegalArgumentException();
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chessBoard.getHeight(); i++) {
            for (int j = 0; j < chessBoard.getWidth(); j++) {
                if (i % 2 == 0) {
                    sb.append("* ");
                } else {
                    sb.append(" *");
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
