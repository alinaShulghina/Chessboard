package com.softserve.edu.chessboard;

/**
 * Created by alin- on 08.12.2017.
 */
public class ChessBoard {
    private int width;
    private int height;

    public ChessBoard(int width, int height) {
        if (width <= 0 | height <= 0) throw new IllegalArgumentException();
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "ChessBoard{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
