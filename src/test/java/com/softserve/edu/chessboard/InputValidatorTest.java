package com.softserve.edu.chessboard;

import com.softserve.edu.chessboard.InputValidator;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by alin- on 12.12.2017.
 */
public class InputValidatorTest {
    @Test(expected = NumberFormatException.class)
    public void validateStringParams() {
        InputValidator.validateParams(new String[]{"Hello", "Kitty"});
    }

    @Test
    public void validateNegativeParams() {
        assertNull(InputValidator.validateParams(new String[]{"-100", "-190"}));
    }

    @Test
    public void validateEmptyParams() {
        assertNull(InputValidator.validateParams(new String[]{}));
    }

    @Test
    public void validate1InputParams() {
        assertNull(InputValidator.validateParams(new String[]{"1"}));
    }

    @Test
    public void validateZeroParams() {
        assertNull(InputValidator.validateParams(new String[]{"0", "0"}));
    }

    @Test
    public void validatePositiveParams() {
        assertArrayEquals(new int[]{23, 34}, InputValidator.validateParams(new String[]{"23", "34"}));
    }
}