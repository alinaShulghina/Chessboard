package com.softserve.edu.chessboard;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alin- on 14.12.2017.
 */
public class ChessboardPrinterTest {
    private ChessBoard chessBoard;
    private ChessboardPrinter printer;

    @Test
    public void printChessboardTestWith1Heigth1Width(){
        chessBoard = new ChessBoard(1,1);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("* \n",printer.printChessboard());
    }

    @Test
    public void printChessboardTestWith10Width10Height() {
        chessBoard = new ChessBoard(10, 10);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("* * * * * * * * * * \n" +
                " * * * * * * * * * *\n" +
                "* * * * * * * * * * \n" +
                " * * * * * * * * * *\n" +
                "* * * * * * * * * * \n" +
                " * * * * * * * * * *\n" +
                "* * * * * * * * * * \n" +
                " * * * * * * * * * *\n" +
                "* * * * * * * * * * \n" +
                " * * * * * * * * * *\n", printer.printChessboard());
    }


    @Test
    public void printChessboardTestWith4Width4Heigth(){
        chessBoard = new ChessBoard(4,4);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("* * * * \n" +
                " * * * *\n" +
                "* * * * \n" +
                " * * * *\n",printer.printChessboard());
    }

    @Test
    public void printChessboardTestWith20Heigth20Width(){
        chessBoard = new ChessBoard(20,20);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n" +
                "* * * * * * * * * * * * * * * * * * * * \n" +
                " * * * * * * * * * * * * * * * * * * * *\n",printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithNegativeNumbers() {
        chessBoard = new ChessBoard(-3, -5);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("", printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithZeroValues(){
        chessBoard = new ChessBoard(0,0);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("",printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithZeroHeight(){
        chessBoard = new ChessBoard(5,0);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("",printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithZeroWidth(){
        chessBoard = new ChessBoard(0,5);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("",printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithNegativeWidth(){
        chessBoard = new ChessBoard(-5,10);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("",printer.printChessboard());
    }

    @Test(expected = IllegalArgumentException.class)
    public void printChessboardTestWithNegativeHeight(){
        chessBoard = new ChessBoard(90,-6);
        printer = new ChessboardPrinter(chessBoard);
        assertEquals("",printer.printChessboard());
    }
}